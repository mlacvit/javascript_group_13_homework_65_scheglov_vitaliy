export class FilmModal {
  constructor(public id: string, public title: string, public poster: string, public video: string, public description: string) {}
}
