import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FilmServices } from '../film.services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FilmModal } from '../film.modal';

@Component({
  selector: 'app-newfilm',
  templateUrl: './newfilm.component.html',
  styleUrls: ['./newfilm.component.css']
})
export class NewfilmComponent implements OnInit, OnDestroy {
  isFetching = false;
  filmFetchingSubscriptions!: Subscription;


  @ViewChild('title') title!: ElementRef;
  @ViewChild('urlImage') urlImage!: ElementRef;
  @ViewChild('urlVideo') urlVideo!: ElementRef;
  @ViewChild('description') description!: ElementRef;

  constructor(private service: FilmServices, private router: Router) { }

  ngOnInit(): void {
    this.filmFetchingSubscriptions = this.service.filmUpLoading.subscribe((isUpLoading => {
      this.isFetching = isUpLoading;
    }))
  }

  sendForm() {
    const title = this.title.nativeElement.value;
    const urlImage = this.urlImage.nativeElement.value;
    const urlVideo = this.urlVideo.nativeElement.value;
    const description = this.description.nativeElement.value;
    const newFilm = new FilmModal('', title, urlImage, urlVideo, description);
    this.service.sendFilms(newFilm).subscribe( () => {
      this.service.getFilm();
    });
  }

  ngOnDestroy(): void {
    this.filmFetchingSubscriptions.unsubscribe();
  }
}
