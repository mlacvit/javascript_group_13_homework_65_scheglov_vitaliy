import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FilmServices } from '../film.services';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-filmtek',
  templateUrl: './filmtek.component.html',
  styleUrls: ['./filmtek.component.css']
})
export class FilmtekComponent implements OnInit {
  filmId = '';
  @ViewChild('iFrameVideo') iFrameVideo!: ElementRef
  constructor(private route: ActivatedRoute, public service: FilmServices, public sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.filmId = params['id'];
      this.service.getFilmOne(this.filmId);
    })

  }

}
