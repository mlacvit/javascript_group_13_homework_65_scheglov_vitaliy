export class CommentModal {
  constructor(public id: string, public name: string, public review: string, public film: string) {}
}
