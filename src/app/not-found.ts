import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>not found!</h1>`,
  styles: [`h1{color: red}`]
})
export class NotFound {

}
