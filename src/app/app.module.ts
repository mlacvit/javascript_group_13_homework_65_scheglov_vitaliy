import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmComponent } from './film/film.component';
import { NewfilmComponent } from './newfilm/newfilm.component';
import { FilmServices } from './film.services';
import { FormsModule } from '@angular/forms';
import { FilmtekComponent } from './filmtek/filmtek.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    NewfilmComponent,
    FilmtekComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [FilmServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
