import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmComponent } from './film/film.component';
import { FilmtekComponent } from './filmtek/filmtek.component';
import { NotFound } from './not-found';
import { NewfilmComponent } from './newfilm/newfilm.component';

const routes: Routes = [
  {path: 'newfilm', component: NewfilmComponent},
  {path: '', component: FilmComponent, children: [
      {path: ':id', component: FilmtekComponent}
    ]},
  {path: '**', component: NotFound},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
