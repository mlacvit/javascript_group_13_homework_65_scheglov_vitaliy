import { Injectable } from '@angular/core';
import { map, Subject, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FilmModal } from './film.modal';
import { CommentModal } from './comment.modal';

@Injectable()
export class FilmServices {
  film: FilmModal[] | null = null;
  filmOne!: FilmModal;
  comment: CommentModal[] | null = null;
  filmChange = new Subject<FilmModal[]>();
  filmFetching = new Subject<boolean>();
  filmUpLoading = new Subject<boolean>();
  filmDeleteUpLoading = new Subject<boolean>();
constructor(private http: HttpClient) {}

  getFilm() {
  this.filmFetching.next(true);
    this.http.get<{ [id: string]: FilmModal }>('https://mlacvit-10af9-default-rtdb.firebaseio.com/films.json')
      .pipe(map(result => {
        if (result === null || undefined) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new FilmModal(
            id,
            data.title,
            data.poster,
            data.video,
            data.description,
          );
        });
      }))
      .subscribe(film => {
        this.film = film;
        this.filmChange.next(this.film.slice());
        this.filmFetching.next(false);
      },error => {
        this.filmFetching.next(false);
      });
  }

  getFilmOne(id: string) {
    this.filmFetching.next(true);
    this.http.get<FilmModal>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/films/${id}.json`)
      .pipe(map(result => {
          return new FilmModal(
            id,
            result.title,
            result.poster,
            result.video,
            result.description,
          );
      }))
      .subscribe(film => {
        this.filmOne = film;
        this.filmFetching.next(false);
      },error => {
        this.filmFetching.next(false);
      });
  }

  sendFilms(film: FilmModal){
  const body = {
    title: film.title,
    poster: film.poster,
    video: film.video,
    description: film.description,
  }
    this.filmUpLoading.next(true);
   return this.http.post('https://mlacvit-10af9-default-rtdb.firebaseio.com/films.json', body).pipe(
     tap(()=> {
       this.filmUpLoading.next(false);
     }, error => {
       this.filmUpLoading.next(false);
     })
   );

  }

  deleteFilm(id: string){
    this.filmDeleteUpLoading.next(true);
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/films/${id}.json`).subscribe();
    this.filmDeleteUpLoading.next(false);
    this.getFilm()
  }

  getComments() {
    this.http.get<{ [id: string]: CommentModal }>('https://mlacvit-10af9-default-rtdb.firebaseio.com/comments.json')
      .pipe(map(result => {
        if (result === null || undefined) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new CommentModal(
            id,
            data.name,
            data.review,
            data.film,
          );
        });
      }))
      .subscribe(Comment => {
        this.comment = Comment;
      });
  }

  sendComments(com: CommentModal){
    const body = {
      name: com.name,
      review: com.review,
      film: com.film,
    }
     this.http.post('https://mlacvit-10af9-default-rtdb.firebaseio.com/comments.json', body).subscribe()
  }
}

