import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FilmServices } from '../film.services';
import { FilmModal } from '../film.modal';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommentModal } from '../comment.modal';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit, OnDestroy {
  films: FilmModal[] | null = null;
  filmChangeSubscriptions!: Subscription;
  filmFetchingSubscriptions!: Subscription;
  filmFetchingSubscriptionsDelete!: Subscription;
  isFetching: boolean = false;
  isFetchingDelete = false;

  @ViewChild('name') name!: ElementRef;
  @ViewChild('review') review!: ElementRef;
  @ViewChild('film') film!: ElementRef;

  constructor(public service: FilmServices) { }

  ngOnInit() {
    this.service.getFilm();
    this.service.getComments();
    this.filmChangeSubscriptions = this.service.filmChange.subscribe((film: FilmModal[]) => {
      this.films = film;
    })
    this.filmFetchingSubscriptions = this.service.filmFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    })
    this.filmFetchingSubscriptionsDelete = this.service.filmDeleteUpLoading.subscribe((isFetching: boolean) => {
      this.isFetchingDelete = isFetching;
    })
  }

  sendComments(){
    const name = this.name.nativeElement.value;
    const film = this.review.nativeElement.value;
    const review = this.film.nativeElement.value;
    const newCom = new CommentModal('', name, film, review);
    this.service.sendComments(newCom);
  }

  ngOnDestroy() {
    this.filmChangeSubscriptions.unsubscribe();
    this.filmFetchingSubscriptions.unsubscribe();
    this.filmFetchingSubscriptionsDelete.unsubscribe();
  }
}
